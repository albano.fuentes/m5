const primerComponente = () => {
  const name = 'Paula'
  const element = <h1>Hola {name}</h1>
  return element;
};

export default primerComponente;
